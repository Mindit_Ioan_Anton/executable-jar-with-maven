package io.mindit.barcode;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class Main {

    private static final String ENCODING = StandardCharsets.UTF_8.name();

    private static String API_URL;
    private static String KEY_NR;

    public static void main(String[] args) throws IOException {

        if (args.length != 2) {
            System.out.println("Not all arguments are set! Please check documentation!");
            return;
        }

        File targetFile = new File(args[1]);
        BufferedReader input = new BufferedReader(new FileReader(args[0]));
        List<String> barCodes = getBarCodesFromInput(input);

        if (barCodes.isEmpty()) {
            System.out.println("Please provide a list of inputs!");
            return;
        }

        HttpClient client = new HttpClient();

        try (OutputStream outStream = new FileOutputStream(targetFile)) {
            for (int i = 0; i < barCodes.size(); i++) {
                String barCode = barCodes.get(i);
                String completeUrl = API_URL + barCode + KEY_NR;
                GetMethod method = new GetMethod(completeUrl);

                client.executeMethod(method);

                writeBarCodeInFile(outStream, barCode, method);

                System.out.printf("processing code %d: %s\n",  i+1, barCode);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private static void writeBarCodeInFile(OutputStream outStream, String barCode, GetMethod method) throws IOException {
        StringWriter writer = new StringWriter();

        IOUtils.copy(method.getResponseBodyAsStream(), writer, ENCODING);
        outStream.write(("Input: " + barCode + "\n").getBytes());
        outStream.write(writer.toString().getBytes());
        outStream.write("\n\n".getBytes());
    }

    private static List<String> getBarCodesFromInput(BufferedReader input) throws IOException {
        List<String> barCodes = new ArrayList<>();
        API_URL = input.readLine();
        KEY_NR = input.readLine();

        //this line is always null, the bar codes always start at 4th line
        input.readLine();

        while (input.ready()) {
            barCodes.add(input.readLine());
        }

        return barCodes;
    }
}
